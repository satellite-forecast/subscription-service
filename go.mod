module safo/subscription-service

go 1.16

require (
	firebase.google.com/go v3.13.0+incompatible
	firebase.google.com/go/v4 v4.8.0
	github.com/auth0/go-jwt-middleware v1.0.1
	github.com/form3tech-oss/jwt-go v3.2.5+incompatible
	github.com/go-openapi/runtime v0.24.1
	github.com/go-resty/resty/v2 v2.7.0
	github.com/gorilla/mux v1.8.0
	github.com/magiconair/properties v1.8.6
	github.com/rs/cors v1.8.2
	github.com/stretchr/testify v1.7.5
	google.golang.org/api v0.85.0
)
