//Copyright (c) 2021 PAULA B. OLMEDO.
//
//This file is part of SUBSCRIPTION-SERVICE
//(see https://gitlab.com/paulaolmedo/subscription-service).
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

// Subscription service
//
// Simple service to manage notifications and subscriptions
//
//     Schemes: http
//     Host: 0.0.0.0:8080
//	   BasePath: /
//     Version: 1.0.0
//	   License: GPLv3 https://www.gnu.org/licenses/gpl-3.0.html
//     Contact: Satellite Forecast<info@satelliteforecast.systems>
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
// swagger:meta
package main

import (
	"errors"
	"log"
	"os"
	server "safo/subscription-service/pkg/server"

	"github.com/magiconair/properties"
)

//go:generate swagger generate spec -m -o ../api/swagger.yml

func main() {
	propertiesPath := os.Getenv(server.ServerProperties)
	smtpConfigPath := os.Getenv(server.SMTPProperties)

	if propertiesPath == "" {
		log.Fatal(errors.New("No server properties file found"))
	} else if smtpConfigPath == "" {
		log.Fatal(errors.New("No SMTP configuration file found"))
	}

	p := properties.MustLoadFile(propertiesPath, properties.UTF8)
	host := p.MustGetString("host")

	environment := p.MustGetString("environment")
	os.Setenv(server.ENVIRONMENT, environment)

	if environment == "production" {
		audience := p.MustGetString("audience")
		issuer := p.MustGetString("issuer")

		os.Setenv(server.AUDIENCE, audience)
		os.Setenv(server.ISSUER, issuer)
	}

	var configuration server.Server
	if err := configuration.InitHTTPServer(); err != nil {
		log.Fatal(err)
	}

	configuration.Run(host)
}
