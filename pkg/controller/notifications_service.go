package controller

import (
	"encoding/json"
	"fmt"

	"firebase.google.com/go/v4/messaging"
)

const (
	subscriptionsTable    = "subscriptions"
	notificationsNode     = "notification"
	errSubscribingToken   = "Error subscribing to topic"
	errUnsubscribingToken = "Error unsubscribing to topic"
)

type serviceProperties struct {
	repository FirebaseRepository
}

type NotificationsService interface {
	CreateSubscription(subscription Subscription) *ModelError
	RetrieveSubscription(email string) (subscriptionToBeStored, *ModelError)
	UpdateSubscription(subscription Subscription) *ModelError
	DeleteSubscription(email, topic string) *ModelError
	SendTopicMessage(topic string, content Content) *ModelError
	GetTopicData() (map[string]StoredPlace, *ModelError)
}

func NewNotificationsService(repository FirebaseRepository) NotificationsService {
	return &serviceProperties{repository}
}

// GetTopicData devuelve la cantidad de suscripciones que tiene cada lugar, y sus respectivos tipos
func (service *serviceProperties) GetTopicData() (map[string]StoredPlace, *ModelError) {
	var object map[string]subscriptionToBeStored

	err := service.repository.Retrieve(&object, subscriptionsTable)
	if err != nil {
		return nil, &ModelError{Summary: "Error while retrieving subscription information", Description: err.Error()}
	}

	places := make(map[string]StoredPlace)

	for _, v := range object {
		notification := v.Notification

		for k1, v1 := range notification {
			aftermathCounter := places[k1].Aftermath
			dailycounter := places[k1].Daily
			recoveryCounter := places[k1].Recovery

			if v1.Aftermath {
				aftermathCounter +=1
			}

			if v1.Daily {
				dailycounter +=1
			}

			if v1.Recovery {
				recoveryCounter +=1
			}
			places[k1] = StoredPlace{Aftermath: aftermathCounter, Daily: dailycounter, Recovery: recoveryCounter}
		}
	}

	return places, nil
}

func (service *serviceProperties) CreateSubscription(incomingSubscription Subscription) *ModelError {
	email := incomingSubscription.Email

	var storedSubscription Subscription
	if err := service.repository.RetrieveSubscription(&storedSubscription, subscriptionsTable, email); err != nil {
		return &ModelError{Summary: "Error while retrieving subscription information", Description: err.Error()}
	}

	information := incomingSubscription.NotificationInformation
	place := information.Place.Name
	isWeb := information.Type.Web

	// TODO hacer esto un toque mas pequeño 
	if storedSubscription.Email != "" {
		// actualiza token nada mas
		if incomingSubscription.Token != storedSubscription.Token {
			token := map[string]interface{}{
				"token": incomingSubscription.Token,
			}

			if err := service.repository.Update(token, subscriptionsTable, email); err != nil {
				return &ModelError{Summary: "Error while updating token subscription", Description: err.Error()}
			}
		}

		if err := service.repository.CreateNotification(information.Place.Name, information, subscriptionsTable, email); err != nil {
			return &ModelError{Summary: "Error while creating subscription", Description: err.Error()}
		}

		if isWeb {
			if err := service.subscribeToken(incomingSubscription); err != nil {
				return err
			}
		}

		return nil
	}

	mapNotification := make(map[string]Notification)
	mapNotification[place] = information

	var newSubscription subscriptionToBeStored
	newSubscription.Email = incomingSubscription.Email
	newSubscription.Token = incomingSubscription.Token
	newSubscription.Notification = mapNotification

	// ver como relacionar tablas en firebase
	// guarda la información de la suscripción en firebase
	if err := service.repository.Create(newSubscription, subscriptionsTable, email); err != nil {
		return &ModelError{Summary: "Error while creating subscription", Description: err.Error()}
	}

	if isWeb {
		service.subscribeToken(incomingSubscription)
	}

	return nil
}

func (service *serviceProperties) UpdateSubscription(updatedSubscription Subscription) *ModelError {
	email := updatedSubscription.Email
	information := updatedSubscription.NotificationInformation
	place := information.Place.Name

	mapNotification := make(map[string]interface{})
	mapNotification[place] = information

	if err := service.repository.UpdateNotification(place, mapNotification, subscriptionsTable, email); err != nil {
		return &ModelError{Summary: "Error updating subscription information", Description: err.Error()}
	}

	if information.Type.Web {
		// formato de tópico -> lugar - tipo_alerta

		aftermath := information.Aftermath
		daily := information.Daily

		aftermathTopic := fmt.Sprintf("%v_aftermath", information.Place.Name)
		dailyTopic := fmt.Sprintf("%v_daily", information.Place.Name)

		if aftermath {
			if err := service.repository.SubscribeToTopic([]string{updatedSubscription.Token}, aftermathTopic); err != nil {
				return &ModelError{Summary: errSubscribingToken, Description: err.Error()}
			}
		} else {
			if err := service.repository.UnsubscribeToTopic([]string{updatedSubscription.Token}, aftermathTopic); err != nil {
				return &ModelError{Summary: errUnsubscribingToken, Description: err.Error()}
			}
		}

		if daily {
			if err := service.repository.SubscribeToTopic([]string{updatedSubscription.Token}, dailyTopic); err != nil {
				return &ModelError{Summary: errSubscribingToken, Description: err.Error()}
			}
		} else {
			if err := service.repository.UnsubscribeToTopic([]string{updatedSubscription.Token}, dailyTopic); err != nil {
				return &ModelError{Summary: errUnsubscribingToken, Description: err.Error()}
			}
		}
	}

	return nil
}

func (service *serviceProperties) DeleteSubscription(email, topic string) *ModelError {
	if err := service.repository.DeleteChild(subscriptionsTable, email, topic); err != nil {
		return &ModelError{Summary: "Error while deleting subscription information", Description: err.Error()}
	}
	return nil
}

func (service *serviceProperties) RetrieveSubscription(email string) (subscriptionToBeStored, *ModelError) {
	var subscription subscriptionToBeStored
	if err := service.repository.RetrieveSubscription(&subscription, subscriptionsTable, email); err != nil {
		return subscription, &ModelError{Summary: "Error while retrieving subscription information", Description: err.Error()}
	}

	return subscription, nil
}

// TODO modificarle el content que no se entiende qué es cada cosa
func (service *serviceProperties) SendTopicMessage(topic string, content Content) *ModelError {
	var mapContent map[string]string
	data, _ := json.Marshal(content)
	if err := json.Unmarshal(data, &mapContent); err != nil {
		return &ModelError{Summary: "Error unmarshalling message content", Description: err.Error()}
	}

	androidTitle := fmt.Sprintf("Alerta para %v", content.Place)
	message := messaging.Message{
		Data:  mapContent,
		Topic: topic,
		Android: &messaging.AndroidConfig{
			Priority:     "high",
			Notification: &messaging.AndroidNotification{Title: androidTitle, Body: content.AlertDescription},
		},
	}

	if err := service.repository.SendMessage(&message); err != nil {
		return &ModelError{Summary: "Error sending message", Description: err.Error()}
	}
	return nil
}

func (service *serviceProperties) subscribeToken(subscription Subscription) *ModelError {
	information := subscription.NotificationInformation
	token := subscription.Token

	// formato de tópico -> lugar - tipo_alerta

	aftermath := information.Aftermath
	daily := information.Daily

	if aftermath {
		topic := fmt.Sprintf("%v_aftermath", information.Place.Name)
		if err := service.repository.SubscribeToTopic([]string{token}, topic); err != nil {
			return &ModelError{Summary: errSubscribingToken, Description: err.Error()}
		}
	}

	if daily {
		topic := fmt.Sprintf("%v_daily", information.Place.Name)
		if err := service.repository.SubscribeToTopic([]string{token}, topic); err != nil {
			return &ModelError{Summary: errSubscribingToken, Description: err.Error()}
		}
	}

	return nil
}
