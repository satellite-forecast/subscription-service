package controller

// EmailBody
//
// Payload necessary to send the email alert
//
// swagger:model
type EmailBody struct {
	// Receiver: array with all the email-subscriptors
	Receiver []string `json:"receiver,omitempty"`
	// Topic: Email-Subject
	Topic string `json:"topic,omitempty"`
	// AlertDescription: brief description of the alert to be sent
	AlertDescription string `json:"description,omitempty"`
	// Place: where the alert occurs
	Place string `json:"place,omitempty"`
	// Coordinates of the place
	Coordinates string `json:"coordinates,omitempty"`
	UrlImagen   string
	UrlPlace    string
}

// EmailConfirmation
//
// Confirmation that the emails were successfully sent
//
// swagger:model
type EmailConfirmation struct {
	// Receiver: array with all the email-subscriptors
	Receiver []string `json:"receiver,omitempty"`
	// Topic: Email-Subject
	Topic string `json:"topic,omitempty"`
}

// Content
//
// Payload necessary to send the push alert
//
// swagger:model
type Content struct {
	// AlertDescription: brief description of the alert to be sent
	AlertDescription string `json:"title,omitempty"`
	// Place: where the alert occurs
	Place string `json:"name,omitempty"`
	// Coordinates of the place
	Coordinates string `json:"coordinates,omitempty"`
}

// Place
//
// Where the alert occurs
//
// swagger:model
type Place struct {
	// Name .
	Name string `json:"name,omitempty"`
	// Coordinates .
	Coordinates string `json:"coordinates,omitempty"` // puede ser un link al mapa
}

// StoredPlace
//
// Where the alert occurs
//
// swagger:model
type StoredPlace struct {
	Aftermath int `json:"aftermath_notification"`
	Recovery int `json:"recovery_notification"`
	Daily int `json:"daily_notification"`
}

// NotificationType
//
// Interface where the notification will be sent
//
// swagger:model
type NotificationType struct {
	// Email: email notification
	Email bool `json:"email"`
	// Web: push notification
	Web bool `json:"web"`
}

// Notification
//
// Payload necessary to manage push subscriptions
//
// swagger:model
type Notification struct {
	// que esto venga de la base de datos cosa de que sea posible agregar nuevos tipos de notificaciones
	// Daily: forecast notification received daily
	Daily bool `json:"daily_notification"`
	// Aftermath: notification after a fire
	Aftermath bool `json:"aftermath_notification"`
	// Recovery: notification of the recovery status
	Recovery bool `json:"recovery_notification"`
	// Type: Email, Push
	Type NotificationType `json:"type"`
	// Place
	Place Place `json:"place"`
}

// Subscription
//
// Payload necessary to manage subscriptions
//
// swagger:model
type Subscription struct {
	// Email: used to identify who's gonna subscribe
	Email string `json:"email"`
	// Token: used to identify the user device to send push notifications
	Token string `json:"token"`
	// NotificationInformation: payload of the subcription itself
	NotificationInformation Notification `json:"notification_information"`
}

type subscriptionToBeStored struct {
	Email        string                  `json:"email"`
	Token        string                  `json:"token"`
	Notification map[string]Notification `json:"notification"`
}

// ModelError
//
// Error
//
// swagger:model
type ModelError struct {
	// Summary of the error
	Summary string `json:"summary"`
	// Description: the error itself
	Description string `json:"description"`
}
