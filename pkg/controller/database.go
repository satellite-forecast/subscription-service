package controller

import (
	"context"
	"errors"
	"fmt"
	"log"
	"os"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
	"firebase.google.com/go/v4/db"
	"firebase.google.com/go/v4/messaging"
	"google.golang.org/api/option"
)

const (
	dbURL       = "https://safo-288221-default-rtdb.firebaseio.com/"
	credentials = "FIREBASE_CREDENTIALS"
)

type FirebaseManagment struct {
	dbClient        *db.Client
	authClient      *auth.Client
	messagingClient *messaging.Client
}

type FirebaseRepository interface {
	Create(object interface{}, mainNode string, child string) error
	CreateNotification(topic string, object interface{}, mainNode string, child string) error
	RetrieveSubscription(object interface{}, mainNode string, child string) error
	Retrieve(object interface{}, table string, child ...string) error
	Update(object map[string]interface{}, mainNode string, child string) error
	UpdateNotification(topic string, object map[string]interface{}, mainNode string, child string) error
	Delete(mainNode string, child string) error
	DeleteChild(table, email, topic string) error
	SubscribeToTopic(token []string, topic string) error
	UnsubscribeToTopic(token []string, topic string) error
	SendMessage(message *messaging.Message) error
}

func AuthenticateFB() *FirebaseManagment {
	ctx := context.Background()
	conf := &firebase.Config{
		DatabaseURL: dbURL,
	}

	fbCredentials := os.Getenv(credentials)
	opt := option.WithCredentialsFile(fbCredentials)

	app, err := firebase.NewApp(ctx, conf, opt)
	if err != nil {
		log.Fatalln("Error initializing app:", err)
	}

	dbClient, err := app.Database(ctx)
	if err != nil {
		log.Fatalln("Error initializing database client:", err)
	}

	authClient, err := app.Auth(ctx)
	if err != nil {
		log.Fatalf("error getting Auth client: %v\n", err)
	}

	messagingClient, err := app.Messaging(ctx)
	if err != nil {
		log.Fatalf("error getting Messaging client: %v\n", err)
	}

	/*esto inicializa la base sin ningún tipo de restricción*/
	return &FirebaseManagment{dbClient: dbClient, authClient: authClient, messagingClient: messagingClient}
}

func (firebaseManagment *FirebaseManagment) Create(object interface{}, mainNode string, child string) error {
	uuid, err := firebaseManagment.getUserUUId(child)
	if err != nil {
		return err
	}

	database := firebaseManagment.dbClient.NewRef(mainNode)
	objectTable := database.Child(uuid)

	if err := objectTable.Set(context.Background(), object); err != nil {
		return err
	}

	return nil
}

func (firebaseManagment *FirebaseManagment) CreateNotification(topic string, object interface{}, mainNode string, child string) error {
	uuid, err := firebaseManagment.getUserUUId(child)
	if err != nil {
		return err
	}

	database := firebaseManagment.dbClient.NewRef(mainNode)
	objectTable := database.Child(uuid)
	notifTable := objectTable.Child(notificationsNode)
	placeTable := notifTable.Child(topic)

	if err := placeTable.Set(context.Background(), object); err != nil {
		return err
	}

	return nil
}

// Retrieve funcion general donde "object" es el objecto a rellenar con el resultado de la consulta, 
// table es la tabla en sí y child es opcional para devolver alguno de los hijos
func (firebaseManagment *FirebaseManagment) Retrieve(object interface{}, table string, child ...string) error {
	database := firebaseManagment.dbClient.NewRef(table)

	// object must be a pointer and not nil
	if err := database.Get(context.Background(), object); err != nil {
		return err
	}

	return nil
}


func (firebaseManagment *FirebaseManagment) RetrieveSubscription(object interface{}, mainNode string, child string) error {
	uuid, err := firebaseManagment.getUserUUId(child)
	if err != nil {
		return err
	}

	database := firebaseManagment.dbClient.NewRef(mainNode)
	objectTable := database.Child(uuid)

	// object must be a pointer and not nil
	if err := objectTable.Get(context.Background(), object); err != nil {
		return err
	}

	return nil
}

func (firebaseManagment *FirebaseManagment) Update(object map[string]interface{}, mainNode string, child string) error {
	uuid, err := firebaseManagment.getUserUUId(child)
	if err != nil {
		return err
	}

	database := firebaseManagment.dbClient.NewRef(mainNode)
	objectTable := database.Child(uuid)

	if err := objectTable.Update(context.Background(), object); err != nil {
		return err
	}

	return nil
}

func (firebaseManagment *FirebaseManagment) UpdateNotification(topic string, object map[string]interface{}, mainNode string, child string) error {
	uuid, err := firebaseManagment.getUserUUId(child)
	if err != nil {
		return err
	}

	database := firebaseManagment.dbClient.NewRef(mainNode)
	objectTable := database.Child(uuid)
	notifTable := objectTable.Child(notificationsNode)
	// placeTable := notifTable.Child(topic)

	if err := notifTable.Update(context.Background(), object); err != nil {
		return err
	}

	return nil
}

func (firebaseManagment *FirebaseManagment) Delete(table string, child string) error {
	uuid, err := firebaseManagment.getUserUUId(child)
	if err != nil {
		return err
	}

	database := firebaseManagment.dbClient.NewRef(table)
	objectTable := database.Child(uuid)

	if err := objectTable.Delete(context.Background()); err != nil {
		return err
	}

	return nil
}

func (firebaseManagment *FirebaseManagment) DeleteChild(table, email, topic string) error {
	main, err := firebaseManagment.getUserUUId(email)
	if err != nil {
		return err
	}

	database := firebaseManagment.dbClient.NewRef(table)
	mainObject := database.Child(main)
	notificationNode := mainObject.Child(notificationsNode)
	notification := notificationNode.Child(topic)

	if err := notification.Delete(context.Background()); err != nil {
		return err
	}

	return nil
}

func (firebaseManagment FirebaseManagment) getUserUUId(email string) (string, error) {
	userRecord, err := firebaseManagment.authClient.GetUserByEmail(context.Background(), email)
	if err != nil {
		return "", err
	}

	return userRecord.UID, nil
}

// tiene que ser un tópico por cada lugar por cada tipo de notificación
// cosquin-seguimiento; cosquin-catastrofe -> o algo así
func (firebaseManagment FirebaseManagment) SubscribeToTopic(token []string, topic string) error {
	response, err := firebaseManagment.messagingClient.SubscribeToTopic(context.Background(), token, topic)
	if err != nil {
		return err
	} else if response.FailureCount != 0 {
		errString := ""
		for i := 0; i < len(response.Errors); i++ {
			errString += response.Errors[i].Reason
			errString += "\n"

		}
		return errors.New(errString)
	}

	fmt.Println(response.SuccessCount, "tokens were subscribed successfully")
	return nil
}

func (firebaseManagment FirebaseManagment) UnsubscribeToTopic(token []string, topic string) error {
	response, err := firebaseManagment.messagingClient.UnsubscribeFromTopic(context.Background(), token, topic)
	if err != nil {
		return err
	}

	fmt.Println(response.SuccessCount, "tokens were unsubscribed successfully")
	return err
}

func (firebaseManagment FirebaseManagment) SendMessage(message *messaging.Message) error {
	response, err := firebaseManagment.messagingClient.Send(context.Background(), message)
	if err != nil {
		return err
	}

	fmt.Println("Successfully sent message", response)
	return err
}
