package server

import (
	"encoding/json"
	"net/http"
	"safo/subscription-service/pkg/controller"
)

func (server *Server) SendPushNotification(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(contentType, appJSON)

	topic := r.URL.Query().Get("topic")
	if topic == "" {
		jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: errQueryParameter})
		return
	}

	var content controller.Content
	err := json.NewDecoder(r.Body).Decode(&content)
	if err != nil {
		// cambiar el contenido de las constantes para que sea mas general
		jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: errDecodingPayload, Description: err.Error()})
		return
	}

	server.Service.SendTopicMessage(topic, content)
}
