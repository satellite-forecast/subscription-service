//Copyright (c) 2021 PAULA B. OLMEDO.
//
//This file is part of SUBSCRIPTION-SERVICE
//(see https://gitlab.com/paulaolmedo/subscription-service).
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

package server

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"safo/subscription-service/pkg/auth"
	"safo/subscription-service/pkg/controller"

	"github.com/go-openapi/runtime/middleware"
	mux "github.com/gorilla/mux"
	cors "github.com/rs/cors"
)

// Server .
type Server struct {
	Router  *mux.Router
	Service controller.NotificationsService
}

// Init configuration
func (server *Server) InitHTTPServer() error {
	server.Router = mux.NewRouter()
	server.InitRouters()
	server.InitDatabase()

	if err := server.Router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		template, err := route.GetPathTemplate()
		if err != nil {
			return fmt.Errorf(errSettingRoutes, err)
		}

		methods, err := route.GetMethods()
		if err != nil {
			return fmt.Errorf(errSettingRoutes, err)
		}

		fmt.Printf("routes %s %s \n", methods, template)

		return nil
	}); err != nil {
		return fmt.Errorf(errInitializingRoutes, err)
	}

	server.InitOpenAPIRouters()

	return nil
}

func (server *Server) InitOpenAPIRouters() {
	if _, err := os.Stat(swaggerpath); err != nil {
		fmt.Printf("cannot open swagger file %v \n", err)
	}

	server.Router.Handle(swaggerpath, http.FileServer(http.Dir("./")))

	if os.Getenv(ENVIRONMENT) == "production" {
		opts := middleware.RedocOpts{SpecURL: swaggerpath}
		redoc := middleware.Redoc(opts, nil)
		server.Router.Handle("/docs", redoc)
	}

	opts := middleware.SwaggerUIOpts{SpecURL: swaggerpath}
	swagger := middleware.SwaggerUI(opts, nil)
	server.Router.Handle("/docs", swagger)
}

// Run .
func (server *Server) Run(host string) {
	fmt.Printf("Listening to: %v \n", host)
	handler := cors.AllowAll().Handler(server.Router)
	log.Fatal(http.ListenAndServe(host, handler))
}

func setMiddlewareWithoutAuthentication(next http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(contentType, appJSON)
		next(w, r)
	}
}

func setMiddlewareWithAuthentication(next http.HandlerFunc) http.HandlerFunc {
	if os.Getenv(ENVIRONMENT) == "production" {
		jwtMiddleware := auth.SetMiddlewareJWT()
		if jwtMiddleware == nil {
			log.Fatal("failed to initialize jwt middleware :(")
		}

		return func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set(contentType, appJSON)
			jwtMiddleware.HandlerWithNext(w, r, next)
		}
	} else { // no es ambiente productivo
		return setMiddlewareWithoutAuthentication(next)
	}
}

func (server *Server) InitDatabase() {
	dao := controller.AuthenticateFB()

	server.Service = controller.NewNotificationsService(dao)
}

// InitRouters .
func (server *Server) InitRouters() {
	// swagger:operation POST /email notification_service SendEmail
	//
	// Send new alert email
	//
	// Given an array of suscriptors to a certain topic, sends an email notification.
	//
	// ---
	// produces:
	// - application/json
	// parameters:
	// - in: query
	//   name: template-path
	//   description: Path where the email template is located
	//   required: true
	//   type: string
	// - in: body
	//   name: Email body
	//   description: Content to be sent
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/EmailBody"
	// responses:
	//   '200':
	//     description: email sucessfully sent
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/EmailConfirmation"
	//   '400':
	//     description: erroneous payload
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	//   '500':
	//     description: internal server error
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	server.Router.HandleFunc("/email", setMiddlewareWithAuthentication(server.SendEmail)).Methods("POST")

	// swagger:operation POST /subscriptions subscription_service RegisterSubscription
	//
	// Registers a subscription request
	//
	//
	// ---
	// produces:
	// - application/json
	// parameters:
	// - in: body
	//   name: Subscription body
	//   description: information of the subscription to be registered
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/Subscription"
	// responses:
	//   '200':
	//     description: subscription sucessfully registered
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/EmailConfirmation"
	//   '400':
	//     description: erroneous payload
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	//   '500':
	//     description: internal server error
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	server.Router.HandleFunc("/subscriptions", setMiddlewareWithAuthentication(server.RegisterSubscription)).Methods("POST")

	// swagger:operation PATCH /notifications subscription_service UpdateSubscription
	//
	// Updates a certain subscription
	//
	//
	// ---
	// produces:
	// - application/json
	// parameters:
	// - in: body
	//   name: subscription body
	//   description: information of the subscription to be registered
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/Subscription"
	// responses:
	//   '200':
	//     description: subscription sucessfully registered
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/EmailConfirmation"
	//   '400':
	//     description: erroneous payload
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	//   '500':
	//     description: internal server error
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	server.Router.HandleFunc("/subscriptions", setMiddlewareWithAuthentication(server.UpdateSubscription)).Methods("PATCH")

	// swagger:operation DELETE /subscriptions subscription_service DeleteSubscription
	//
	// Deletes a subscription
	//
	//
	// ---
	// produces:
	// - application/json
	// parameters:
	// - in: query
	//   name: email
	//   description: User email
	//   required: true
	//   type: string
	// - in: query
	//   name: topic
	//   description: notification to unsuscribe
	//   required: true
	//   type: string
	// responses:
	//   '200':
	//     description: subscription sucessfully registered
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/EmailConfirmation"
	//   '400':
	//     description: missing query param
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	//   '500':
	//     description: Internal Server Error
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	server.Router.HandleFunc("/subscriptions", setMiddlewareWithAuthentication(server.DeleteSubscription)).Methods("DELETE")

	// swagger:operation GET /subscriptions subscription_service RetrieveSubscription
	//
	// Retrieves subscriptions
	//
	//
	// ---
	// produces:
	// - application/json
	// parameters:
	// - in: query
	//   name: email
	//   description: User email
	//   required: true
	//   type: string
	// - in: query
	//   name: places
	//   description: whether it's gonna to retrieve only the places where the user is subscripted
	//   required: false
	//   type: string
	// responses:
	//   '200':
	//     description: subscription sucessfully registered
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/EmailConfirmation"
	//   '400':
	//     description: erroneous payload, or missing query param
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	//   '500':
	//     description: missing query param
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	server.Router.HandleFunc("/subscriptions", setMiddlewareWithAuthentication(server.RetrieveSubscription)).Methods("GET")

	// swagger:operation GET /subscriptions/information subscription_service GetSubscriptionsInformation
	//
	// Retrieves subscriptions information
	//
	//
	// ---
	// produces:
	// - application/json
	// responses:
	//   '200':
	//     description: subscriptions sucessfully retrieved
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/EmailConfirmation"
	//   '500':
	//     description: missing query param
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	server.Router.HandleFunc("/subscriptions/information", setMiddlewareWithAuthentication(server.GetSubscriptionsInformation)).Methods("GET")

	// swagger:operation POST /web notification_service SendPushNotification
	//
	// Send new web notification
	//
	//
	// ---
	// produces:
	// - application/json
	// parameters:
	// - in: query
	//   name: topic
	//   description: Topic message
	//   required: true
	//   type: string
	// - in: body
	//   name: Message content
	//   description: Content to be sent
	//   required: true
	//   schema:
	//     "$ref": "#/definitions/Content"
	// responses:
	//   '200':
	//     description: Email sucessfully sent
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/EmailConfirmation"
	//   '400':
	//     description: Erroneous payload
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	//   '500':
	//     description: Internal Server Error
	//     schema:
	//       type: array
	//       items:
	//         "$ref": "#/definitions/ModelError"
	server.Router.HandleFunc("/web", setMiddlewareWithAuthentication(server.SendPushNotification)).Methods("POST")
}
