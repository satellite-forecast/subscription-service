package server

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	// ENV VARIABLES
	ServerProperties = "ES_PROPERTIES"
	SMTPProperties   = "SMTP_CONFIGURATION"
	AUDIENCE         = "AUDIENCE"
	ISSUER           = "ISSUER"
	ENVIRONMENT      = "ENVIRONMENT"

	// HEADERS
	contentType = "Content-Type"
	appJSON     = "application/json; charset=UTF-8"
	appTXT      = "text/html"
	appMIME     = "MIME-version: 1.0"
	path        = "template-path"

	// ROUTES
	errSettingRoutes      = "error setting routes -> %v"
	errInitializingRoutes = "error when initializing routes -> %w"

	// EMAIL SERVICE
	errQueryParameter  = "Missing query parameter"
	errInvalidValue    = "[%v] is not a valid value for this query"
	errDecodingPayload = "Error decoding email payload"
	errReadingTemplate = "Error reading template"
	errSendingEmail    = "Error sending email"

	swaggerpath = "/swagger.yml"
)

// jsonResponse builds the endpoints response
func jsonResponse(w http.ResponseWriter, statusCode int, response ...interface{}) {
	w.WriteHeader(statusCode)

	err := json.NewEncoder(w).Encode(response)
	if err != nil {
		fmt.Fprintf(w, "%s", err.Error())
	}
}
