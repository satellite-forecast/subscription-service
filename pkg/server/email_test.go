package server_test

import (
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	controller "safo/subscription-service/pkg/controller"
	server "safo/subscription-service/pkg/server"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const (
	templatePath      = "../../templates/template.html"
	templateDummyPath = "../../templates/template_missing.html"

	email         = "/email"
	notifications = "/subscriptions"
)

var (
	testserver        *httptest.Server
	testConfiguration server.Server
)

func TestMain(test *testing.M) {
	os.Setenv("SMTP_CONFIGURATION", "../../config/email.properties")
	fb_credentials := os.Getenv("FB_CONFIG")
	os.Setenv("FIREBASE_CREDENTIALS", fb_credentials)
	InitService()
	os.Exit(test.Run())
}

func InitService() {
	testConfiguration = server.Server{}
	if err := testConfiguration.InitHTTPServer(); err != nil {
		log.Fatal(err)
	}
	testserver = httptest.NewServer(testConfiguration.Router)
}

func TestMissingQueryParameter(t *testing.T) {
	emailBody := controller.EmailBody{}
	client := resty.New()

	response, err := client.R().
		SetBody(emailBody).
		EnableTrace().
		Post(testserver.URL + email)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Missing query parameter\",\"description\":\"\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusBadRequest

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestEmptyPayload(t *testing.T) {
	client := resty.New()

	response, err := client.R().
		SetQueryParams(map[string]string{
			"template-path": templatePath,
		}).
		EnableTrace().
		Post(testserver.URL + email)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error decoding email payload\",\"description\":\"EOF\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusBadRequest

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestMissingTemplateFile(t *testing.T) {
	payload := controller.EmailBody{
		Receiver:         []string{"abc@server.com"},
		Topic:            "subject",
		AlertDescription: "some alert",
		Place:            "some place",
		Coordinates:      "some coordinates",
	}
	client := resty.New()

	response, err := client.R().
		SetQueryParams(map[string]string{
			"template-path": templateDummyPath,
		}).
		SetBody(payload).
		EnableTrace().
		Post(testserver.URL + email)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error reading template\",\"description\":\"Error while parsing template: stat ../../templates/template_missing.html: no such file or directory\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusBadRequest

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestErrorSendingEmailDueMissingCredentials(t *testing.T) {
	payload := controller.EmailBody{
		Receiver:         []string{"abc@server.com"},
		Topic:            "subject",
		AlertDescription: "some alert",
		Place:            "some place",
		Coordinates:      "some coordinates",
	}
	client := resty.New()

	response, err := client.R().
		SetQueryParams(map[string]string{
			"template-path": templatePath,
		}).
		SetBody(payload).
		EnableTrace().
		Post(testserver.URL + email)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error sending email\",\"description\":\"535 5.7.8 Error: authentication failed: \"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusInternalServerError

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}
