package server_test

import (
	"net/http"
	"safo/subscription-service/pkg/controller"
	"testing"

	"github.com/go-resty/resty/v2"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestRegisterSubscriptionWithEmptyPayload(t *testing.T) {
	client := resty.New()
	var subscription interface{}

	response, err := client.R().
		EnableTrace().
		SetBody(subscription).
		Post(testserver.URL + notifications)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error decoding subscription payload\",\"description\":\"EOF\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusBadRequest

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestRegisterSubscriptionDueMissingUser(t *testing.T) {
	client := resty.New()

	subscription := controller.Subscription{
		Email: "missing.user@server.com",
		Token: "",
		NotificationInformation: controller.Notification{
			Place: controller.Place{
				Name:        "someplace",
				Coordinates: "",
			},
		},
	}

	response, err := client.R().
		EnableTrace().
		SetBody(subscription).
		Post(testserver.URL + notifications)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error while retrieving subscription information\",\"description\":\"no user exists with the email: \\\"missing.user@server.com\\\"\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusInternalServerError

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestRegisterSubscriptionDueInvalidCharacters(t *testing.T) {
	t.Skip()
	client := resty.New()

	subscription := controller.Subscription{
		Email: "someuser@server.com",
		Token: "",
		NotificationInformation: controller.Notification{
			Daily: true,
			Place: controller.Place{
				Name:        "°namewithinvalid@characters",
				Coordinates: "",
			},
		},
	}

	response, err := client.R().
		EnableTrace().
		SetBody(subscription).
		Post(testserver.URL + notifications)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error reading place name\",\"description\":\"should contain only letters.\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusBadRequest

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestRegisterSubscriptionDueMissingPlaceName(t *testing.T) {
	client := resty.New()

	subscription := controller.Subscription{
		Email: "someuser@server.com",
		Token: "",
		NotificationInformation: controller.Notification{
			Daily: true,
		},
	}

	response, err := client.R().
		EnableTrace().
		SetBody(subscription).
		Post(testserver.URL + notifications)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Place information should be provided\",\"description\":\"\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusBadRequest

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestRegisterSubscriptionDueMissingToken(t *testing.T) {
	client := resty.New()

	subscription := controller.Subscription{
		Email: "someuser@server.com",
		Token: "",
		NotificationInformation: controller.Notification{
			Daily: true,
			Place: controller.Place{
				Name:        "someplace",
				Coordinates: "",
			},
			Type: controller.NotificationType{Web: true},
		},
	}

	response, err := client.R().
		EnableTrace().
		SetBody(subscription).
		Post(testserver.URL + notifications)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error reading token information\",\"description\":\"in order to subscribe to push notificaations, please provide a token\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusBadRequest

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestRegisterSubscriptionDueInvalidToken(t *testing.T) {
	client := resty.New()

	subscription := controller.Subscription{
		Email: "paulabeatrizolmedo@gmail.com",
		Token: "invalid_token",
		NotificationInformation: controller.Notification{
			Daily: true,
			Place: controller.Place{
				Name:        "someplace",
				Coordinates: "",
			},
			Type: controller.NotificationType{Web: true},
		},
	}

	response, err := client.R().
		EnableTrace().
		SetBody(subscription).
		Post(testserver.URL + notifications)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error subscribing to topic\",\"description\":\"INVALID_ARGUMENT\\n\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusInternalServerError

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestDeleteSubscriptionOfMissingUser(t *testing.T) {
	client := resty.New()

	response, err := client.R().
		SetQueryParams(map[string]string{
			"email": "missing.user@server.com",
			"topic": "some_topic",
		}).
		EnableTrace().
		Delete(testserver.URL + notifications)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error while deleting subscription information\",\"description\":\"no user exists with the email: \\\"missing.user@server.com\\\"\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusInternalServerError

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}

func TestRetrieveSubscriptionOfMissingUser(t *testing.T) {
	client := resty.New()

	response, err := client.R().
		SetQueryParams(map[string]string{
			"email": "missing.user@server.com",
		}).
		EnableTrace().
		Get(testserver.URL + notifications)

	require.NoError(t, err)

	actualResponse := string(response.Body())
	expectedResponse := "[{\"summary\":\"Error while retrieving subscription information\",\"description\":\"no user exists with the email: \\\"missing.user@server.com\\\"\"}]\n"

	actualStatusCode := response.StatusCode()
	expectedStatusCode := http.StatusInternalServerError

	assert.Equal(t, expectedResponse, actualResponse)
	assert.Equal(t, expectedStatusCode, actualStatusCode)
}
