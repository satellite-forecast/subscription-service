//Copyright (c) 2021 PAULA B. OLMEDO.
//
//This file is part of SUBSCRIPTION-SERVICE
//(see https://gitlab.com/paulaolmedo/subscription-service).
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

package server

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/smtp"
	"os"

	controller "safo/subscription-service/pkg/controller"

	"github.com/magiconair/properties"
)

var errParsingTemplate = errors.New("Error while parsing template")

type smtpProperties struct {
	host   string
	port   string
	sender string
	pwd    string
	to     []string
}

func (server *Server) SendEmail(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(contentType, appJSON)

	templatePath := r.URL.Query().Get(path)
	if templatePath == "" {
		jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: errQueryParameter})
		return
	}

	var emailBody controller.EmailBody
	err := json.NewDecoder(r.Body).Decode(&emailBody)
	if err != nil {
		jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: errDecodingPayload, Description: err.Error()})
		return
	}

	if err := sendEmail(emailBody, templatePath); err != nil {
		if errors.Is(err, errParsingTemplate) {
			jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: errReadingTemplate, Description: err.Error()})
			return
		}

		jsonResponse(w, http.StatusInternalServerError, controller.ModelError{Summary: errSendingEmail, Description: err.Error()})
		return
	}

	jsonResponse(w, http.StatusCreated, controller.EmailConfirmation{Receiver: emailBody.Receiver, Topic: emailBody.Topic})
}

func parseTemplate(templatePath string, emailBody controller.EmailBody, sender string) (bytes.Buffer, error) {
	var body bytes.Buffer

	if _, err := os.Stat(templatePath); err != nil {
		return body, err
	}

	t := template.Must(template.ParseFiles(templatePath))

	header := make(map[string]string)
	header["From"] = sender
	header["Subject"] = emailBody.Topic
	header["MIME-Version"] = "1.0"
	header["Content-Type"] = appTXT

	message := ""
	for k, v := range header {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}

	body.Write([]byte(message))
	emailBody.UrlImagen = fmt.Sprintf("https://image-loader-lld32gxwaq-uc.a.run.app/images/processed?filename=%v.png", emailBody.Place)
	emailBody.UrlPlace = fmt.Sprintf("https://www.google.com/maps/@?api=1&map_action=map&center=%v", emailBody.Coordinates)

	if err := t.Execute(&body, emailBody); err != nil {
		return body, err
	}

	return body, nil
}

func sendEmail(emailBody controller.EmailBody, templatePath string) error {
	p := properties.MustLoadFile(os.Getenv(SMTPProperties), properties.UTF8)
	host := p.MustGetString("host")
	port := p.MustGetString("port")
	sender := p.MustGetString("sender")
	pwd := p.MustGetString("pwd")

	if host == "" || port == "" {
		return errors.New("Host information missing")
	} else if sender == "" || pwd == "" {
		return errors.New("Sender information missing")
	}

	body, err := parseTemplate(templatePath, emailBody, sender)
	if err != nil {
		return fmt.Errorf("%w: %v", errParsingTemplate, err.Error())
	}

	smtpProperties := smtpProperties{host: host, port: port, sender: sender, pwd: pwd, to: emailBody.Receiver}

	return tcpEmail(smtpProperties, body)
}

func tcpEmail(smtpProperties smtpProperties, body bytes.Buffer) error {
	server := smtpProperties.host

	tlsConfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         server,
	}
	hostInformation := smtpProperties.host + ":" + smtpProperties.port
	connection, err := tls.Dial("tcp", hostInformation, tlsConfig)
	if err != nil {
		return err
	}

	c, err := smtp.NewClient(connection, server)
	if err != nil {
		log.Fatal(err)
		return err
	}

	auth := smtp.PlainAuth("", smtpProperties.sender, smtpProperties.pwd, smtpProperties.host)
	if err = c.Auth(auth); err != nil {
		return err
	}

	if err = c.Mail(smtpProperties.sender); err != nil {
		return err
	}

	for _, addr := range smtpProperties.to {
		if err = c.Rcpt(addr); err != nil {
			return err
		}
	}

	w, err := c.Data()
	if err != nil {
		return err
	}

	_, err = w.Write(body.Bytes())
	if err != nil {
		return err
	}

	err = w.Close()
	if err != nil {
		return err
	}

	err = c.Quit()
	if err != nil {
		return err
	}

	return nil
}
