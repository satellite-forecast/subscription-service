//Copyright (c) 2021 PAULA B. OLMEDO.
//
//This file is part of SUBSCRIPTION-SERVICE
//(see https://gitlab.com/paulaolmedo/subscription-service).
//
//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program. If not, see <http://www.gnu.org/licenses/>.

package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	controller "safo/subscription-service/pkg/controller"
)

func (server *Server) RegisterSubscription(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(contentType, appJSON)

	var subscription controller.Subscription
	if err := validateSubscriptionPayload(r, &subscription); err != nil {
		jsonResponse(w, http.StatusBadRequest, err)
		return
	}

	if err := server.Service.CreateSubscription(subscription); err != nil {
		jsonResponse(w, http.StatusInternalServerError, err)
		return
	}

	jsonResponse(w, http.StatusCreated, "Subscription correctly registered.")
}

func (server *Server) RetrieveSubscription(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(contentType, appJSON)

	email := r.URL.Query().Get("email")
	if email == "" {
		jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: errQueryParameter})
		return
	}

	subscription, err := server.Service.RetrieveSubscription(email)
	if err != nil {
		jsonResponse(w, http.StatusInternalServerError, err)
		return
	}
	places := r.URL.Query().Get("places")
	var subscriptedPlaces []string
	if strings.ToLower(places) == "true" {
		for k := range subscription.Notification {
			subscriptedPlaces = append(subscriptedPlaces, k)
		}

		jsonResponse(w, http.StatusOK, subscriptedPlaces)
		return
	} else if strings.ToLower(places) != "false" {
		jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: fmt.Sprintf(errInvalidValue, places)})
		return
	}
	jsonResponse(w, http.StatusOK, subscription)
}

func (server *Server) UpdateSubscription(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(contentType, appJSON)

	var subscription controller.Subscription
	if err := validateSubscriptionPayload(r, &subscription); err != nil {
		jsonResponse(w, http.StatusBadRequest, err)
		return
	}

	if err := server.Service.UpdateSubscription(subscription); err != nil {
		jsonResponse(w, http.StatusInternalServerError, err)
		return
	}
}

func (server *Server) DeleteSubscription(w http.ResponseWriter, r *http.Request) {
	w.Header().Set(contentType, appJSON)

	email := r.URL.Query().Get("email")
	if email == "" {
		jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: errQueryParameter})
		return
	}

	topic := r.URL.Query().Get("topic")
	if topic == "" {
		jsonResponse(w, http.StatusBadRequest, controller.ModelError{Summary: errQueryParameter})
		return
	}

	if err := server.Service.DeleteSubscription(email, topic); err != nil {
		jsonResponse(w, http.StatusInternalServerError, err)
		return
	}
}

func (server *Server) GetSubscriptionsInformation(w http.ResponseWriter, r *http.Request){
	w.Header().Set(contentType, appJSON)
	places, err := server.Service.GetTopicData()
	if err != nil {
		jsonResponse(w, http.StatusInternalServerError, err)
		return
	}
	jsonResponse(w, http.StatusOK, places)
}

func validateSubscriptionPayload(request *http.Request, subscription *controller.Subscription) *controller.ModelError {
	err := json.NewDecoder(request.Body).Decode(&subscription)
	if err != nil {
		return &controller.ModelError{Summary: "Error decoding subscription payload", Description: err.Error()}
	}

	if (subscription.NotificationInformation.Place == controller.Place{}) {
		return &controller.ModelError{Summary: "Place information should be provided"}
	}

	//Saco esta validación por ahora hasta tener una regex mejor
	/*
		isStringAlphabetic := regexp.MustCompile(`^[a-zA-Z]+$`).MatchString
		if !isStringAlphabetic(subscription.NotificationInformation.Place.Name) {
			return &controller.ModelError{Summary: "Error reading place name", Description: "should contain only letters."}
		}*/

	if subscription.Token == "" && subscription.NotificationInformation.Type.Web {
		return &controller.ModelError{Summary: "Error reading token information", Description: "in order to subscribe to push notificaations, please provide a token"}
	}

	return nil
}
