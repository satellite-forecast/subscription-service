# Subscription service 📧🛰️

## How to build w/ docker 🐳

Keep in mind that the following files are needed:
- app-production.properties: file to store the [Auth0](https://auth0.com) properties
- email.properties: file to store the SMTP configuration. Currently this implementation uses a [Titan server](https://support.titan.email/hc/en-us/categories/360002428994-Get-Started) is being used, but it should work with other servers.
- fb_private_key.json: [Firebase messaging](https://firebase.google.com/docs/cloud-messaging) keys
- swagger.yml: openAPI specification generated with [go-swagger](https://goswagger.io/)



```
docker build -f build/Dockerfile -t subscription-service .
```

## How to run

### docker 🐳

```
docker run --rm -p 8080:8080 subscription-service
```

### without docker 💻

```
ES_PROPERTIES=config/app-production.properties SMTP_CONFIGURATION=config/email.properties FIREBASE_CREDENTIALS=fb_private_key.json go run cmd/main.go
```

## API documentation:

- [Gitlab pages](https://satellite-forecast.gitlab.io/subscription-service/)